# Google Cloud CI Components

GitLab [CI/CD components](https://docs.gitlab.com/ee/ci/components) for the Google Cloud integration.

## Docker Image Mirroring

Use this component to mirror a Docker image from the GitLab Container Registry to Google Artifact Registry.

To utilize this component, add it to an existing `.gitlab-ci.yml` file using the `include:` keyword:

```yaml
include:
  - component: gitlab.com/components/google-cloud/ar-docker-mirror@<VERSION>
```

where `<VERSION>` is the latest released tag or `main`.

This adds a job called `ar-docker-mirror` to the pipeline.

### Prerequisites

To use this component you must first enable and configure the Google Artifact Registry integration for your project. See [TODO](link) for instructions.

### Inputs

| Input              | Default Value | Description                                                                                                                                                                          |
| ------------------ | ------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `stage`            | `build`       | The CI stage where you want to add the job.                                                                                                                                          |
| `needs`            |               | Specify the CI job that the component should wait for before starting. Set this to the name of the job where your Docker image is built and pushed to the GitLab Container Registry. |
| `gar_project`      |               | The target GCP project ID.                                                                                                                                                           |
| `gar_repository`   |               | The name of the target Google Artifact Registry repository.                                                                                                                          |
| `gar_location`     |               | The GCP location of the target Google Artifact Registry repository.                                                                                                                  |
| `source_image`     |               | The source Docker image hosted in the GitLab Container Registry.                                                                                                                     |
| `target_image`     |               | Custom name and tag for the Docker image to be pushed to the Google Artifact Registry, relative to the repository path.                                                              |

TODO: Remove `gar_project` and make `gar_repository` and `gar_location` optional once https://gitlab.com/gitlab-org/gitlab/-/issues/425153 is complete.

#### Example

```yaml
# First we build and push our Docker image to the GitLab Container Registry (no changes)
build:
  image: docker:24.0.6
  stage: build
  services:
    - docker:24.0.6-dind
  variables:
    GITLAB_IMAGE: registry.gitlab.com/my-group/my-project/app:latest
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -t $GITLAB_IMAGE .
    - docker push $GITLAB_IMAGE

# Then we include this component so that this image is mirrored to Google Artifact Registry
include:
  - component: gitlab.com/components/google-cloud/ar-docker-mirror@~latest
    inputs:
      stage: build
      needs: build
      gar_project: my-gcp-project-9abafed1
      gar_repository: production
      gar_location: us-east1
      source_image: registry.gitlab.com/my-group/my-project/app:latest
      target_image: app:latest # resolves to: us-east1-docker.pkg.dev/my-gcp-project-9abafed1/production/app:latest
```

## Contribute

Please read about CI/CD components and best practices at https://docs.gitlab.com/ee/ci/components.